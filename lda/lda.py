#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Latent Dirichlet Allocation + collapsed Gibbs sampling
# This code is available under the MIT License.
# (c)2010-2011 Nakatani Shuyo / Cybozu Labs Inc.

import numpy
import numpy as numpy
from nltk import tokenize
import heapq
from pymongo import MongoClient

class LDA:
    def __init__(self, K, alpha, beta, docs, V, smartinit=True):
        self.K = K
        self.alpha = alpha # parameter of topics prior
        self.beta = beta   # parameter of words prior
        self.docs = docs
        self.V = V

        self.z_m_n = [] # topics of words of documents
        self.n_m_z = numpy.zeros((len(self.docs), K)) + alpha     # word count of each document and topic
        self.n_z_t = numpy.zeros((K, V)) + beta # word count of each topic and vocabulary
        self.n_z = numpy.zeros(K) + V * beta    # word count of each topic

        self.N = 0
        for m, doc in enumerate(docs):
            self.N += len(doc)
            z_n = []
            for t in doc:
                if smartinit:
                    p_z = self.n_z_t[:, t] * self.n_m_z[m] / self.n_z
                    z = numpy.random.multinomial(1, p_z / p_z.sum()).argmax()
                else:
                    z = numpy.random.randint(0, K)
                z_n.append(z)
                self.n_m_z[m, z] += 1
                self.n_z_t[z, t] += 1
                self.n_z[z] += 1
            self.z_m_n.append(numpy.array(z_n))

    def inference(self):
        """learning once iteration"""
        for m, doc in enumerate(self.docs):
            z_n = self.z_m_n[m]
            n_m_z = self.n_m_z[m]
            for n, t in enumerate(doc):
                # discount for n-th word t with topic z
                z = z_n[n]
                n_m_z[z] -= 1
                self.n_z_t[z, t] -= 1
                self.n_z[z] -= 1

                # sampling topic new_z for t
                p_z = self.n_z_t[:, t] * n_m_z / self.n_z
                new_z = numpy.random.multinomial(1, p_z / p_z.sum()).argmax()

                # set z the new topic and increment counters
                z_n[n] = new_z
                n_m_z[new_z] += 1
                self.n_z_t[new_z, t] += 1
                self.n_z[new_z] += 1

    def worddist(self):
        """get topic-word distribution"""
        return self.n_z_t / self.n_z[:, numpy.newaxis]

    def perplexity(self, docs=None):
        if docs == None: docs = self.docs
        phi = self.worddist()
        log_per = 0
        N = 0
        Kalpha = self.K * self.alpha
        for m, doc in enumerate(docs):
            theta = self.n_m_z[m] / (len(self.docs[m]) + Kalpha)
            for w in doc:
                log_per -= numpy.log(numpy.inner(phi[:,w], theta))
            N += len(doc)
        return numpy.exp(log_per / N)

def lda_learning(lda, iteration, voca, ids, docs):
    pre_perp = lda.perplexity()
    print "initial perplexity=%f" % pre_perp
    for i in range(iteration):
        lda.inference()
        perp = lda.perplexity()
        print "-%d p=%f" % (i + 1, perp)
        if pre_perp:
            if pre_perp < perp:
                output_word_topic_dist(lda, voca)
                pre_perp = None
            else:
                pre_perp = perp
    output_word_topic_dist(lda, voca, ids, docs)

def output_word_topic_dist(lda, voca, ids, docs):
    zcount = numpy.zeros(lda.K, dtype=int)
    wordcount = [dict() for k in xrange(lda.K)]
    for xlist, zlist in zip(lda.docs, lda.z_m_n):
        for x, z in zip(xlist, zlist):
            zcount[z] += 1
            if x in wordcount[z]:
                wordcount[z][x] += 1
            else:
                wordcount[z][x] = 1

    phi = lda.worddist()
    #for k in xrange(lda.K):
        #print "\n-- topic: %d (%d words)" % (k, zcount[k])
        #for w in numpy.argsort(-phi[k])[:30]:
            #print "%s: %f (%d)" % (voca[w], phi[k,w], wordcount[k].get(w,0))

    #Insert topics into the database
    client = MongoClient('mongodb://' + "localhost" + ':' + str(27017) + '/')
    db = client['testdb']
    
    lda_topics = db['lda_topics_1']
    try:
        lda_topics.remove()
    except:
        if 1==0: print 0
    for k in xrange(lda.K):
        lda_topics.insert({'_id': k})
        for w in numpy.argsort(-phi[k])[:200]:
            a = [str(w),str(phi[k,w])]
            lda_topics.find_one({'_id':k})
            lda_topics.update( {'_id':k}, {"$push": {'words' : a }}, upsert=True)
    print "topics inserted"

    #Insert Vocabulary into the database
    vocab = db['vocab_1']
    try:
        vocab.remove()
    except:
        if 1==0: print 0
    for w in range(0,voca.size()):
        vocab.insert({'_id': w, 'word': voca[w]})
        for k in xrange(lda.K):
            vocab.update( {'_id': w}, {"$push": {'topic_map' : str(lda.n_z_t[k,w]) }}, upsert=True)
    print "vocab inserted"


    theta = lda.n_m_z[:,:]
    for i in range(0,theta.shape[0]):
        theta[i,:] = theta[i,:] / theta[i,:].sum()

    phi = lda.n_z_t[:,:]
    for i in range(0,voca.size()):
        phi[:,i] = phi[:,i] / phi[:,i].sum()

    #Insert topic mapping values
    articles = db['articles_1']
    try:
        articles.update( {'topic_map': { '$exists': True } }, { '$unset': { 'topic_map' : True  } })
        articles.update( {'word_cloud': { '$exists': True } }, { '$unset': { 'word_cloud' : True  } })
    except:
        if 1==1: print 1
    for k in range(0,lda.n_m_z.shape[0]):
        #articles.update( { '_id': ids[k] }, { '$unset': { 'topic_map': 1 } } )
        doc = set(docs[k])
        for j in range(0,lda.n_m_z.shape[1]):
            a = [str(j),str(lda.n_m_z[k][j])]
            articles.update( {'_id': ids[k]}, {"$push": {'topic_map' : a }}, upsert=True)
        for words in doc:
            a = [str(words),str(numpy.dot(theta[k,:],phi[:,words]).sum())]
            articles.update( {'_id': ids[k]}, {"$push": {'word_cloud' : a }}, upsert=True)
    print "articles - topics inserted"

# heuristics .. best 2 topics, best 3 words, 20%ile and 10%ile weightage sentences
def summary_article(doc_num, theta, phi, voca):
    import vocabulary
    sentences = []
    line = vocabulary.load_collection_doc('localhost', 27017, 'testdb', 'articles_1', doc_num)
    sentences = tokenize.sent_tokenize(line.replace("\n","").replace("\r",""))
    topic_num = heapq.nlargest(2, range(0,theta.shape[1]), key=lambda i:theta[doc_num,i])
    score_sentences_1 = summary_topic_article(topic_num[0], doc_num, sentences, theta, phi, voca)
    score_sentences_2 = summary_topic_article(topic_num[1], doc_num, sentences, theta, phi, voca)
    summary = heapq.nlargest(numpy.int(len(score_sentences_1)*0.2), range(0,len(score_sentences_1)), key=lambda i:score_sentences_1[i])
    summary.extend(heapq.nlargest(numpy.int(len(score_sentences_2)*0.1), range(0,len(score_sentences_2)), key=lambda i:score_sentences_2[i]))
    summary.sort()
    summary_final = []
    for i in set(summary):
        summary_final.append(sentences[i])
    return summary_final

def summary_topic_article(topic_num, doc_num, sentences, theta, phi, voca):
    import vocabulary
    clean_sentences = []
    for i in range(0,len(sentences)):
        clean_sentences.append(voca.doc_to_ids( vocabulary.load_string(sentences[i]) ))
    score_sentences = []
    for i in range(0,len(clean_sentences)):
        a = []
        for j in range(0,len(clean_sentences[i])):
            a.append(phi[topic_num,clean_sentences[i][j]])
        b = heapq.nlargest(3,a)
        c = 1
        for j in range(0,len(b)):
            c = c * b[j]
        score_sentences.append(c*theta[doc_num,topic_num])
    return score_sentences

def main():
    import optparse
    import vocabulary
    parser = optparse.OptionParser()
    parser.add_option("-f", dest="filename", help="corpus filename")
    parser.add_option("-d", dest="coll", help="collection yes or no")
    parser.add_option("-c", dest="corpus", help="using range of Brown corpus' files(start:end)")
    parser.add_option("--alpha", dest="alpha", type="float", help="parameter alpha", default=0.5)
    parser.add_option("--beta", dest="beta", type="float", help="parameter beta", default=0.5)
    parser.add_option("-k", dest="K", type="int", help="number of topics", default=20)
    parser.add_option("-i", dest="iteration", type="int", help="iteration count", default=100)
    parser.add_option("-s", dest="smartinit", action="store_true", help="smart initialize of parameters", default=False)
    parser.add_option("--stopwords", dest="stopwords", help="exclude stop words", action="store_true", default=False)
    parser.add_option("--seed", dest="seed", type="int", help="random seed")
    parser.add_option("--df", dest="df", type="int", help="threshold of document freaquency to cut words", default=0)
    parser.add_option("--summary", dest="summary", type="int", help="summarize or not", default=0)
    (options, args) = parser.parse_args()
    if not (options.filename or options.corpus or options.coll): parser.error("need corpus filename(-f) or corpus range(-c)")

    if options.filename:
        corpus = vocabulary.load_file(options.filename)
    elif options.coll == 'Y':
        print "loading from collection"
        (ids, corpus) = vocabulary.load_collection('localhost', 27017, 'testdb', 'articles_1')
        print "corpus loaded"
    else:
        corpus = vocabulary.load_corpus(options.corpus)
        if not corpus: parser.error("corpus range(-c) forms 'start:end'")
    if options.seed != None:
        numpy.random.seed(options.seed)

    voca = vocabulary.Vocabulary(options.stopwords)
    docs = [voca.doc_to_ids(doc) for doc in corpus]
    if options.df > 0: docs = voca.cut_low_freq(docs, options.df)

    lda = LDA(options.K, options.alpha, options.beta, docs, voca.size(), options.smartinit)
    print "corpus=%d, words=%d, K=%d, a=%f, b=%f" % (len(corpus), len(voca.vocas), options.K, options.alpha, options.beta)

    #import cProfile
    #cProfile.runctx('lda_learning(lda, options.iteration, voca)', globals(), locals(), 'lda.profile')
    lda_learning(lda, options.iteration, voca, ids, docs)

    if options.summary == 1:
        theta = lda.n_m_z[:,:]
        for i in range(0,theta.shape[0]):
            theta[i,:] = theta[i,:] / theta[i,:].sum()

        phi = lda.n_z_t[:,:]
        for i in range(0,voca.size()):
            phi[:,i] = phi[:,i] / phi[:,i].sum()

        f = open("./output/word_cloud.txt","w")
        for i in range(0,len(docs)):
            doc = set(docs[i])
            f.write("Document Number " + str(i) + "\n")
            for words in doc:
                f.write(voca[words] + " " + str(numpy.dot(theta[i,:],phi[:,words]).sum()) + "\n")
            f.write("\n ----------------------------------- \n")

        f.close()

        f = open("./output/topic_mapping","w")
        for doc_num in range(0,theta.shape[0]):
            topic_num = heapq.nlargest(10, range(0,theta.shape[1]), key=lambda i:theta[doc_num,i])
            f.write( str(doc_num) + "|\n" )
            for j in range(0,10):
                f.write( str(topic_num[j]) + ":" + str(theta[doc_num][topic_num[j]]) )
            	f.write("\n")

        f.close()

        f = open("./output/summary.txt","w")
        for i in range(0,theta.shape[0]):
            summary = summary_article(i,theta,phi,voca)
            for j in range(0,len(summary)):
                f.write(summary[j]+" ")
            f.write("\n\n")
    
        f.close()


if __name__ == "__main__":
    main()

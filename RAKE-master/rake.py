# Implementation of RAKE - Rapid Automtic Keyword Exraction algorithm
# as described in:
# Rose, S., D. Engel, N. Cramer, and W. Cowley (2010). 
# Automatic keyword extraction from indi-vidual documents. 
# In M. W. Berry and J. Kogan (Eds.), Text Mining: Applications and Theory.unknown: John Wiley and Sons, Ltd.

import re
import operator
import math
from nltk.tag.stanford import POSTagger
from nltk.tokenize import word_tokenize
from pymongo import MongoClient

st = POSTagger('/home/mridul/Downloads/stanford-postagger-2013-06-20/models/english-bidirectional-distsim.tagger','/home/mridul/Downloads/stanford-postagger-2013-06-20/stanford-postagger-3.2.0.jar')
debug = False
test = True

def isnum (s):
    try:
    	float(s) if '.' in s else int(s)
        return True
    except ValueError:
        return False

# Utility function to load stop words from a file and return as a list of words
# @param stopWordFile Path and file name of a file containing stop words.
# @return list A list of stop words.
def loadStopWords(stopWordFile):
    stopWords = []
    for line in open(stopWordFile):
        if (line.strip()[0:1] != "#"):
            for word in line.split( ): #in case more than one per line
                stopWords.append(word)
    return stopWords

# Utility function to return a list of all words that are have a length greater than a specified number of characters.
# @param text The text that must be split in to words.
# @param minWordReturnSize The minimum no of characters a word must have to be included.
def separatewords(text,minWordReturnSize):
	splitter=re.compile('[^a-zA-Z0-9_\\+\\-/]')
	words = []
	for singleWord in splitter.split(text):
		currWord = singleWord.strip().lower()
		#leave numbers in phrase, but don't count as words, since they tend to invlate scores of their phrases
		if len(currWord)>minWordReturnSize and currWord != '' and not isnum(currWord): 
			words.append(currWord)
	return words

# Utility function to return a list of sentences.
# @param text The text that must be split in to sentences.
def splitSentences(text):
	#removed - and ' from the sentencedelimiters list
	sentenceDelimiters = re.compile(u'[.!?,;:\t\\"\\(\\)\\\u2019\u2013]')
	sentenceList = sentenceDelimiters.split(text)
	return sentenceList

def buildStopwordRegExPattern(pathtostopwordsfile):
	stopwordlist = loadStopWords(pathtostopwordsfile)
	stopwordregexlist = []
	for wrd in stopwordlist:
		wrdregex = '[^ ]*\\b' + wrd + '\\b[^ ]*'
		stopwordregexlist.append(wrdregex)
	stopwordpattern = re.compile('|'.join(stopwordregexlist), re.IGNORECASE)
	return stopwordpattern

def generateCandidateKeywords(sentenceList, stopwordpattern):
	phraseList = []
	for s in sentenceList:
		pos_s = st.tag(word_tokenize(s))
		tmp = re.sub(stopwordpattern, '|', s.strip())
		count = 0
		phrases = tmp.split("|")
		#print tmp
		#print phrases
		for phrase in phrases:
			#print pos_s
			phrase = phrase.strip().lower()
			if (phrase!=""):
				#print phrase
				#print s
				#print pos_s
				strt = count
				end = count + len(phrase.split()) - 1
				pList = []
				flag = strt
				#print strt
				#print end
				if len(phrase.split()) > 1:
					for i in range(strt,end+1):
						if pos_s[i][1][:2] == 'VB':
							pList.append(phrase.rsplit(' ', end-i+1)[0])
							if (i!=strt): pList.append(pos_s[i][0])
							phrase = phrase.split(' ',i-strt+1)[-1]
							#print phrase
							flag = i
					if flag!=end:
						pList.append(phrase) #.split(' ',flag-strt+1)[-1])
					#if pos_s[strt][1][:2] == 'VB':
					#	phrase = phrase.split(' ', 1)[1]
					#if pos_s[end][1][:2] == 'VB':
					#	phrase = phrase.rsplit(' ', 1)[0]
					#print pList
					phraseList.extend(pList)
				else:
					phraseList.append(phrase)
			count = count + len(phrase.split()) + 1
	return phraseList

def generateCandidateKeywordsManning(text, sentenceList, stopwordpattern):
	phraseList = []
	try:
		pos_s = st.tag(separatewords(text.lower(),0))
		#for s in sentenceList:
			#pos_s = st.tag(word_tokenize(s))
		for i in range(0, len(pos_s)-2):
			if pos_s[i][1][:2] == 'JJ' and pos_s[i+1][1][:2] == 'JJ' and pos_s[i+2][1][:2] == 'NN':
				phraseList.append(pos_s[i][0] + " " + pos_s[i+1][0] + " " + pos_s[i+2][0])
			elif pos_s[i][1][:2] == 'JJ' and pos_s[i+1][1][:2] == 'NN':
				phraseList.append(pos_s[i][0] + " " + pos_s[i+1][0])
				if pos_s[i+2][1][:2] == 'NN':
					phraseList.append(pos_s[i][0] + " " + pos_s[i+1][0] + " " + pos_s[i+2][0])
			elif pos_s[i][1][:2] == 'NN' and pos_s[i+1][1][:2] == 'JJ' and pos_s[i+2][1][:2] == 'NN':
				phraseList.append(pos_s[i][0] + " " + pos_s[i+1][0] + " " + pos_s[i+2][0])
			elif pos_s[i][1][:2] == 'NN' and pos_s[i+1][1][:2] == 'NN':
				phraseList.append(pos_s[i][0] + " " + pos_s[i+1][0])
				if pos_s[i+2][1][:2] == 'NN':
					phraseList.append(pos_s[i][0] + " " + pos_s[i+1][0] + " " + pos_s[i+2][0])
		return phraseList
	except:
		print "problem with POS tagging"
		return []

def calculateWordScores(phraseList):
	wordfreq = {}
	worddegree = {}
	for phrase in phraseList:
		wordlist = separatewords(phrase,0) 
		wordlistlength = len(wordlist)
		wordlistdegree = wordlistlength - 1
		#if wordlistdegree > 3: wordlistdegree = 3 #exp.
		for word in wordlist:
			wordfreq.setdefault(word,0)
			wordfreq[word] += 1
			worddegree.setdefault(word,0)
			worddegree[word] += wordlistdegree #orig.
			#worddegree[word] += 1/(wordlistlength*1.0) #exp.
	for item in wordfreq:
		worddegree[item] = worddegree[item]+wordfreq[item] 	

	# Calculate Word scores = deg(w)/frew(w)
	wordscore = {}
	for item in wordfreq:
		wordscore.setdefault(item,0)
		wordscore[item] = worddegree[item]/(wordfreq[item] * 1.0) #orig.
		#wordscore[item] = wordfreq[item]/(worddegree[item] * 1.0) #exp.
	return wordscore
	
def generateCandidateKeywordScores(phraseList, wordscore):
	keywordcandidates = {}
	for phrase in phraseList:
		keywordcandidates.setdefault(phrase,0)
		wordlist = separatewords(phrase,0) 
		candidatescore = 0
		for word in wordlist:
			candidatescore += wordscore[word]
		keywordcandidates[phrase] = candidatescore
	return keywordcandidates

def process(text):
	# Split text into sentences
	sentenceList = splitSentences(text)
	#stoppath = "FoxStoplist.txt" #Fox stoplist contains "numbers", so it will not find "natural numbers" like in Table 1.1
	stoppath = "SmartStoplist.txt" #SMART stoplist misses some of the lower-scoring keywords in Figure 1.5, which means that the top 1/3 cuts off one of the 4.0 score words in Table 1.1
	stopwordpattern = buildStopwordRegExPattern(stoppath)

	# generate candidate keywords
	phraseList = generateCandidateKeywordsManning(text, sentenceList, stopwordpattern)

	# calculate individual word scores
	wordscores = calculateWordScores(phraseList)

	# generate candidate keyword scores
	keywordcandidates = generateCandidateKeywordScores(phraseList, wordscores)
	if debug: print keywordcandidates

	sortedKeywords = sorted(keywordcandidates.iteritems(), key=operator.itemgetter(1), reverse=True)
	if debug: print sortedKeywords

	totalKeywords = len(sortedKeywords)
	if debug: 
		print totalKeywords
		for i in range(0,totalKeywords/3):
			print sortedKeywords[i]
		print st.tag(word_tokenize(text))

	return sortedKeywords

def main():
	import optparse
	parser = optparse.OptionParser()
	parser.add_option("-f", dest="filename", help="corpus filename")
	parser.add_option("-d", dest="coll", help="collection yes or no")
	(options, args) = parser.parse_args()
	if options.filename:
		f = open(options.filename, 'r')																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																		
		text = f.read()
		print text
		f.close()
		print process(text)
	elif options.coll == 'Y':
		print "loading from collection"
		client = MongoClient('mongodb://' + 'localhost' + ':' + str(27017) + '/')
		db = client['testdb']
		articles = db['articles']
		print articles
		for article in articles.find():
			try:
				if article['keyphrases_manning'] != '':
					doc = ''
			except:
				doc = article['content']
				print article['_id']
				#print article['id_id']
				#print article['content']
			if len(doc)>0:
				key_phrases = process(doc)
				for phrase in key_phrases:
					articles.update( {'_id':article['_id']}, {"$push": {'keyphrases_manning' : phrase}}, upsert=True)
		print "corpus loaded"
	else:
		print "Please provide the file"
		return 0


if __name__ == "__main__":
	main()

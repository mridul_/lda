import re
import operator
import math
from nltk.tag.stanford import POSTagger
from nltk.tokenize import word_tokenize
from pymongo import MongoClient
import socket
import json
import urllib

import DBUtils


def isnum(s):
    try:
        float(s) if '.' in s else int(s)
        return True
    except ValueError:
        return False

# Utility function to return a list of all words that are have a length greater than a specified number of characters.
# @param text The text that must be split in to words.
# @param minWordReturnSize The minimum no of characters a word must have to be included.
def separatewords(text, minWordReturnSize):
    #mapping wouldn't to would and other apostrophes will be split about ' : you've = you ve
    '''text = text.replace("n't"," not")
    text = text.replace("'s"," is")
    text = text.replace("'n"," and")
    text = text.replace("'d"," would")
    text = text.replace("'ll"," will")
    text = text.replace("'ve"," have")
    text = text.replace("'m"," am")
    text = text.replace("'re'"," are")
    '''
    splitter = re.compile('[^a-zA-Z0-9_\\+\\-\']')
    words = []
    for singleWord in splitter.split(text):
        currWord = singleWord.strip().lower()
        #leave numbers in phrase, but don't count as words, since they tend to invlate scores of their phrases
        if len(currWord) > minWordReturnSize and currWord != '' and not isnum(currWord):
            words.append(currWord)
    return words

# Utility function to return a list of sentences.
# @param text The text that must be split in to sentences.
def splitSentences(text):
    #removed - and ' from the sentencedelimiters list
    sentenceDelimiters = re.compile(u'[.!?,;:\t\\"\\(\\)\\\u2019\u2013]')
    sentenceList = sentenceDelimiters.split(text)
    return sentenceList


stplst = ["a", "as", "able", "about", "above", "according", "accordingly", "across", "actually", "after", "afterwards",
          "again", "against", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also",
          "although", "always", "am", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone",
          "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "arent",
          "around", "as", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "b", "be",
          "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being",
          "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by",
          "c", "cmon", "cs", "came", "can", "cant", "cannot", "cant", "cause", "causes", "certain", "certainly",
          "changes", "clearly", "co", "com", "come", "comes", "concerning", "consequently", "consider", "considering",
          "contain", "containing", "contains", "corresponding", "could", "couldnt", "course", "currently", "d",
          "definitely", "described", "despite", "did", "didnt", "different", "do", "does", "doesnt", "doing", "dont",
          "done", "down", "downwards", "during", "e", "each", "edu", "eg", "eight", "either", "else", "elsewhere",
          "enough", "entirely", "especially", "et", "etc", "even", "ever", "every", "everybody", "everyone",
          "everything", "everywhere", "ex", "exactly", "example", "except", "f", "far", "few", "fifth", "first", "five",
          "followed", "following", "follows", "for", "former", "formerly", "forth", "four", "from", "further",
          "furthermore", "g", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got",
          "gotten", "greetings", "h", "had", "hadnt", "happens", "hardly", "has", "hasnt", "have", "havent", "having",
          "he", "hes", "hello", "help", "hence", "her", "here", "heres", "hereafter", "hereby", "herein", "hereupon",
          "hers", "herself", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "i",
          "id", "ill", "im", "ive", "ie", "if", "ignored", "immediate", "in", "inasmuch", "inc", "indeed", "indicate",
          "indicated", "indicates", "inner", "insofar", "instead", "into", "inward", "is", "isnt", "it", "itd", "itll",
          "its", "its", "itself", "j", "just", "k", "keep", "keeps", "kept", "know", "knows", "known", "l", "last",
          "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely",
          "little", "look", "looking", "looks", "ltd", "m", "mainly", "many", "may", "maybe", "me", "mean", "meanwhile",
          "merely", "might", "more", "moreover", "most", "mostly", "much", "must", "my", "myself", "n", "name",
          "namely", "nd", "near", "nearly", "necessary", "need", "needs", "neither", "never", "nevertheless", "new",
          "next", "nine", "no", "nobody", "non", "none", "noone", "nor", "normally", "not", "nothing", "novel", "now",
          "nowhere", "o", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones",
          "only", "onto", "or", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside",
          "over", "overall", "own", "p", "particular", "particularly", "per", "perhaps", "placed", "please", "plus",
          "possible", "presumably", "probably", "provides", "q", "que", "quite", "qv", "r", "rather", "rd", "re",
          "really", "reasonably", "regarding", "regardless", "regards", "relatively", "respectively", "right", "s",
          "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed",
          "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several",
          "shall", "she", "should", "shouldnt", "since", "six", "so", "some", "somebody", "somehow", "someone",
          "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify",
          "specifying", "still", "sub", "such", "sup", "sure", "t", "ts", "take", "taken", "tell", "tends", "th",
          "than", "thank", "thanks", "thanx", "that", "thats", "thats", "the", "their", "theirs", "them", "themselves",
          "then", "thence", "there", "theres", "thereafter", "thereby", "therefore", "therein", "theres", "thereupon",
          "these", "they", "theyd", "theyll", "theyre", "theyve", "think", "third", "this", "thorough", "thoroughly",
          "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "took",
          "toward", "towards", "tried", "tries", "truly", "try", "trying", "twice", "two", "u", "un", "under",
          "unfortunately", "unless", "unlikely", "until", "unto", "up", "upon", "us", "use", "used", "useful", "uses",
          "using", "usually", "uucp", "v", "value", "various", "very", "via", "viz", "vs", "w", "want", "wants", "was",
          "wasnt", "way", "we", "wed", "well", "were", "weve", "welcome", "well", "went", "were", "werent", "what",
          "whats", "whatever", "when", "whence", "whenever", "where", "wheres", "whereafter", "whereas", "whereby",
          "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whos", "whoever", "whole",
          "whom", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wont", "wonder", "would",
          "would", "wouldnt", "x", "y", "yes", "yet", "you", "youd", "youll", "youre", "youve", "your", "yours",
          "yourself", "yourselves", "z", "zero"]


def createSentencesAndPosTags(articles):
    #sentenceList is the list of all the sentences in a document. sentenceLists is the list of all the sentenceList for different documents
    sentenceLists = []
    pos_wts = []
    for article in articles:
        #print "createSentencesAndPosTags " + str(article['_id'])
        pos_wt = []
        doc = article['content']
        if len(doc) > 0:
            sentenceLists.append(splitSentences(doc.lower()))
            ln_words = separatewords(doc, 0)
            ln = ""
            for wrds in ln_words:
                ln = ln + " " + wrds
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(("localhost", 8080))
            #print ln
            sock.sendall(ln + "\n")
            data = sock.recv(4096)
            #print data
            data_split = data.split()
            for a in data_split:
                pos_wt.append(a.split(","))
            pos_wts.append(pos_wt)
            sock.close()
            #pos_wts.append(st.tag(separatewords(doc,0)))
            #print pos_wtsif pha
    return (sentenceLists, pos_wts)


def markPhrases(doc, phrases, delim):
    wrds_mod = doc.split()
    #just need to make sure that the phrases are in decreasing order of length otherwise it
    #will highlight New York Stock Exchange as New York and Stock Exchange
    for phrase in phrases:
        w_len = len(phrase.split())
        wrds = wrds_mod
        wrds_mod = []
        i = 0
        while i < len(wrds) - w_len + 1:
            tmp = ""
            for j in range(0, w_len):
                tmp = tmp + " " + wrds[i + j]
            if phrase.strip().lower() in tmp.strip().lower() and delim not in tmp.strip():
                wrds_mod.append(delim + tmp.strip().replace(" ", delim) + delim)
                print i
                i = i + w_len
            else:
                wrds_mod.append(wrds[i])
                i = i + 1
    doc_r = ""
    for w in wrds_mod:
        doc_r = doc_r + " " + w
    return doc_r


def main():
    import optparse
    import ner

    parser = optparse.OptionParser()
    parser.add_option("-f", dest="filename", help="corpus filename")
    parser.add_option("-d", dest="coll", help="collection yes or no")
    (options, args) = parser.parse_args()
    if options.filename:
        f = open(options.filename, 'r')
        doc = f.read()
        print doc
        f.close()
        tagger = ner.SocketNER(host='localhost', port=8081)
        try:
            orgs = tagger.get_entities(doc)["ORGANIZATION"]
            pers = tagger.get_entities(doc)["PERSON"]
            locs = tagger.get_entities(doc)["LOCATION"]
            print orgs
            nas = []
            nas[:] = orgs[:]
            nas.extend(pers)
            nas.extend(locs)
            nas = list(set(nas))
            print nas
        except BaseException as e:
            print "Error", e
    elif options.coll == 'Y':
        print "loading from collection"

        db = DBUtils.connect_db()

        articles = db.articles
        api_key = 'AIzaSyB4RxyL-cEewgxcTumD92BvHfgTkz4Z4FE'
        print articles
        for article in articles.find():
            w = []
            try:
                if article['namedentities_NER'] != '':
                    print str(article['_id']) + " done"
                    doc = ''
            except:
                # doc = article['content']
                # doc = doc.replace("&amp;", "&")
                # doc = re.sub(r'http://[^ ]+', '', doc)
                # doc = re.sub(r'www.[^ ]+', '', doc)
                content = article['content']
                doc = ""
                for para in content['paras']:
                    if para['head'] != -1:
                        doc += '\n' + content['heads'][para['head']] + '\n'
                    doc += ' '.join(para['cont'])

            if len(doc) > 0:
                article_id = article['_id']
                print article_id

                tagger = ner.SocketNER(host='localhost', port=8081)
                try:
                    def get_or_else(value):
                        return value if value is not None else []

                    entities = tagger.get_entities(doc)
                    orgs = get_or_else(entities.get('ORGANIZATION'))
                    pers = get_or_else(entities.get('PERSON'))
                    locs = get_or_else(entities.get('LOCATION'))
                    """
                    for org in orgs:
                        print org + " ORGANIZATION"
                    for per in pers:
                        print per + " PERSON"
                    for loc in locs:
                        print loc + " LOCATION"
                    """
                    nas = []
                    nas[:] = orgs[:]
                    nas.extend(pers)
                    nas.extend(locs)
                    nas = list(set(nas))
                    print len(nas)

                    DBUtils.update_orgs(db, article_id, orgs)
                    DBUtils.update_people(db, article_id, pers)
                    DBUtils.update_locs(db, article_id, locs)

                    print "Done"

                    # for na in nas:
                    #     articles.update({'_id': article_id}, {"$push": {'namedentities_NER': na}}, upsert=True)
                    #     #print doc
                    #     #print markPhrases(doc,orgs.extend(per).extend(loc),"|IA-NER|")
                except BaseException as e:
                    print "Exception hit!", e

                """
                a = []
                a.append(article)
                sentenceLists,pos_wts = createSentencesAndPosTags(a)
                if len(pos_wts) > 0:
                    pos_wt = pos_wts[0]
                else:
                    pos_wt = []

                for i in range(0,len(pos_wt)-1):
                    if pos_wt[i][0] != '' and pos_wt[i][1][:2] in ('NN'):
                        query = pos_wt[i][0]
                        print query
                        service_url = 'https://www.googleapis.com/freebase/v1/search'
                        params = {
                            'query': query,
                            'key': api_key
                        }
                        url = service_url + '?' + urllib.urlencode(params)
                        response = []
                        response = json.loads(urllib.urlopen(url).read())
                        for result in response['result']:
                            print result['name'] + ' (' + str(result['score']) + ')'

                print "\n\n\n\n"

                phrases = []
                for w1 in w:
                    phrases.extend(processLikelihoodRatios(a,w1,sentenceLists,pos_wts))
                phrases.sort(key=lambda phrases:phrases[1],reverse=True)
                for phrase in phrases:
                    print phrase
                    #articles.update( {'_id':article['_id']}, {"$push": {'namedentities_NER' : phrase}}, upsert=True)
                    """
    else:
        print "Please provide the file"
        return 0


if __name__ == "__main__":
    main()

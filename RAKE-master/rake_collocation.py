# Implementation of RAKE - Rapid Automtic Keyword Exraction algorithm
# as described in:
# Rose, S., D. Engel, N. Cramer, and W. Cowley (2010). 
# Automatic keyword extraction from indi-vidual documents. 
# In M. W. Berry and J. Kogan (Eds.), Text Mining: Applications and Theory.unknown: John Wiley and Sons, Ltd.

import re
import operator
import math
from time import sleep
from nltk.tag.stanford import POSTagger
from nltk.tokenize import word_tokenize
from pymongo import MongoClient
import socket
import json
import urllib

import DBUtils

from multiprocessing import Pool, Process

#st = POSTagger('/home/mridul/Downloads/stan/stanford-postagger-2013-06-20/models/english-bidirectional-distsim.tagger','/home/mridul/stan/Downloads/stanford-postagger-2013-06-20/stanford-postagger-3.2.0.jar')
debug = False
test = True


def isnum(s):
    try:
        float(s) if '.' in s else int(s)
        return True
    except ValueError:
        return False

# Utility function to return a list of all words that are have a length greater than a specified number of characters.
# @param text The text that must be split in to words.
# @param minWordReturnSize The minimum no of characters a word must have to be included.
def separatewords(text, minWordReturnSize):
    #mapping wouldn't to would and other apostrophes will be split about ' : you've = you ve
    text = text.replace("n't", " not")
    text = text.replace("'s", " is")
    text = text.replace("'n", " and")
    text = text.replace("'d", " would")
    text = text.replace("'ll", " will")
    text = text.replace("'ve", " have")
    text = text.replace("'m", " am")
    text = text.replace("'re'", " are")
    splitter = re.compile('[^a-zA-Z0-9_\\+\\-\']')
    words = []
    for singleWord in splitter.split(text):
        currWord = singleWord.strip()
        #leave numbers in phrase, but don't count as words, since they tend to invlate scores of their phrases
        if len(currWord) > minWordReturnSize and currWord != '' and not isnum(currWord):
            words.append(currWord)
    return words

# Utility function to return a list of sentences.
# @param text The text that must be split in to sentences.
def splitSentences(text):
    #removed - and ' from the sentencedelimiters list
    sentenceDelimiters = re.compile(u'[.!?,;:\t\\"\\(\\)\\\u2019\u2013]')
    sentenceList = sentenceDelimiters.split(text)
    return sentenceList


def generateMeanVariance(sentenceList, w1, w2):
    phraseList = []
    dist = []
    for s in sentenceList:
        wt = word_tokenize(s)
        w1_in = [i for i, x in enumerate(wt) if x == w1]
        w2_in = [i for i, x in enumerate(wt) if x == w2]
        if w1_in != [] and w2_in != []:
            for i in range(0, len(w1_in)):
                for j in range(0, len(w2_in)):
                    if abs(w1_in[i] - w2_in[j]) < 9:
                        dist.append(w1_in[i] - w2_in[j])
                        #print w1_in[i]-w2_in[j]
    return dist


def processMeanVariance(articles):
    # Split text into sentences
    w = ["fusion", "io", "server", "virtual", "performance", "vmware", "statement", "accelerator", "mubarak",
         "iomemory", "complaint", "dell", "activist", "booth", "hp", "software", "ibm", "virtualized", "vmworld",
         "workload", "medium", "application", "fusionio", "machine", "vsphere", "checkpoint", "acceleration", "leading",
         "muslim", "press", "date", "accelerate", "ion", "vmmark", "caching", "recently", "announced", "committed",
         "aug", "forward", "gaza", "shared", "vitacost", "featuring", "ucs", "quoted", "health", "support", "release",
         "report", "environment", "offered", "operating", "processor", "power", "lower", "expectation", "risk",
         "expected", "starting", "proliant", "ioturbine", "circumstance", "vdi", "benchmark", "cafe", "improves",
         "previous", "history", "broad", "running", "flash", "claiming", "accelerates", "virtualizing", "rolled",
         "successful", "working", "offer", "back"]
    for i, w1 in enumerate(w):
        for w2 in w[i + 1:]:
            key_phrases = []
            for article in articles.find():
                doc = article['content']
                if len(doc) > 0:
                    #instead of using lower() use separatewords .. FIGURE OUT
                    sentenceList = splitSentences(doc.lower())
                    phraseList = generateMeanVariance(sentenceList, w1, w2)
                    key_phrases.extend(phraseList)
            if key_phrases != []:
                mean = sum(key_phrases) / len(key_phrases)
                key_phrases = [x - mean for x in key_phrases]
                variance = math.sqrt(sum([x * y for x, y in zip(key_phrases, key_phrases)])) / len(key_phrases)
                print w1 + " " + w2 + " " + str(mean) + " " + str(variance)
            else:
                print w1 + " " + w2


stplst = ["a", "as", "able", "about", "above", "according", "accordingly", "across", "actually", "after", "afterwards",
          "again", "against", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also",
          "although", "always", "am", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone",
          "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "arent",
          "around", "as", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "b", "be",
          "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being",
          "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by",
          "c", "cmon", "cs", "came", "can", "cant", "cannot", "cant", "cause", "causes", "certain", "certainly",
          "changes", "clearly", "co", "com", "come", "comes", "concerning", "consequently", "consider", "considering",
          "contain", "containing", "contains", "corresponding", "could", "couldnt", "course", "currently", "d",
          "definitely", "described", "despite", "did", "didnt", "different", "do", "does", "doesnt", "doing", "dont",
          "done", "down", "downwards", "during", "e", "each", "edu", "eg", "eight", "either", "else", "elsewhere",
          "enough", "entirely", "especially", "et", "etc", "even", "ever", "every", "everybody", "everyone",
          "everything", "everywhere", "ex", "exactly", "example", "except", "f", "far", "few", "fifth", "first", "five",
          "followed", "following", "follows", "for", "former", "formerly", "forth", "four", "from", "further",
          "furthermore", "g", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got",
          "gotten", "greetings", "h", "had", "hadnt", "happens", "hardly", "has", "hasnt", "have", "havent", "having",
          "he", "hes", "hello", "help", "hence", "her", "here", "heres", "hereafter", "hereby", "herein", "hereupon",
          "hers", "herself", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "i",
          "id", "ill", "im", "ive", "ie", "if", "ignored", "immediate", "in", "inasmuch", "inc", "indeed", "indicate",
          "indicated", "indicates", "inner", "insofar", "instead", "into", "inward", "is", "isnt", "it", "itd", "itll",
          "its", "its", "itself", "j", "just", "k", "keep", "keeps", "kept", "know", "knows", "known", "l", "last",
          "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely",
          "little", "look", "looking", "looks", "ltd", "m", "mainly", "many", "may", "maybe", "me", "mean", "meanwhile",
          "merely", "might", "more", "moreover", "most", "mostly", "much", "must", "my", "myself", "n", "name",
          "namely", "nd", "near", "nearly", "necessary", "need", "needs", "neither", "never", "nevertheless", "new",
          "next", "nine", "no", "nobody", "non", "none", "noone", "nor", "normally", "not", "nothing", "novel", "now",
          "nowhere", "o", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones",
          "only", "onto", "or", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside",
          "over", "overall", "own", "p", "particular", "particularly", "per", "perhaps", "placed", "please", "plus",
          "possible", "presumably", "probably", "provides", "q", "que", "quite", "qv", "r", "rather", "rd", "re",
          "really", "reasonably", "regarding", "regardless", "regards", "relatively", "respectively", "right", "s",
          "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed",
          "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several",
          "shall", "she", "should", "shouldnt", "since", "six", "so", "some", "somebody", "somehow", "someone",
          "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify",
          "specifying", "still", "sub", "such", "sup", "sure", "t", "ts", "take", "taken", "tell", "tends", "th",
          "than", "thank", "thanks", "thanx", "that", "thats", "thats", "the", "their", "theirs", "them", "themselves",
          "then", "thence", "there", "theres", "thereafter", "thereby", "therefore", "therein", "theres", "thereupon",
          "these", "they", "theyd", "theyll", "theyre", "theyve", "think", "third", "this", "thorough", "thoroughly",
          "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "took",
          "toward", "towards", "tried", "tries", "truly", "try", "trying", "twice", "two", "u", "un", "under",
          "unfortunately", "unless", "unlikely", "until", "unto", "up", "upon", "us", "use", "used", "useful", "uses",
          "using", "usually", "uucp", "v", "value", "various", "very", "via", "viz", "vs", "w", "want", "wants", "was",
          "wasnt", "way", "we", "wed", "well", "were", "weve", "welcome", "well", "went", "were", "werent", "what",
          "whats", "whatever", "when", "whence", "whenever", "where", "wheres", "whereafter", "whereas", "whereby",
          "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whos", "whoever", "whole",
          "whom", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wont", "wonder", "would",
          "would", "wouldnt", "x", "y", "yes", "yet", "you", "youd", "youll", "youre", "youve", "your", "yours",
          "yourself", "yourselves", "z", "zero"]


def createSentencesAndPosTags_old(articles):
    #sentenceList is the list of all the sentences in a document. sentenceLists is the list of all the sentenceList for different documents
    sentenceLists = []
    pos_wts = []
    for article in articles:
        #print "createSentencesAndPosTags " + str(article['_id'])
        pos_wt = []
        doc = article['content']
        if len(doc) > 0:
            doc = re.sub(r'http://[^ ]+', '', doc)
            doc = re.sub(r'www.[^ ]+', '', doc)
            sentenceLists.append(splitSentences(doc.lower()))
            ln_words = separatewords(doc, 0)
            ln = ""
            for wrds in ln_words:
                ln = ln + " " + wrds
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(("localhost", 8080))
            #print ln
            sock.sendall(ln + "\n")
            data = sock.recv(999999999)
            #print data
            data_split = data.split()
            for a in data_split:
                pos_wt.append(a.split(","))
            pos_wts.append(pos_wt)
            sock.close()
            #print pos_wt
            #pos_wts.append(st.tag(separatewords(doc,0)))
            #print pos_wts
    return (sentenceLists, pos_wts)


def createSentencesAndPosTags(articles):
    #sentenceList is the list of all the sentences in a document. sentenceLists is the list of all the sentenceList for different documents
    sentenceLists = []
    pos_wts = []
    for article in articles:
        #print "createSentencesAndPosTags " + str(article['_id'])
        doc = get_sane_content(article['content'])
        if len(doc) > 0:
            sentenceLists.append(article['sents'])
            pos_wts.append(article['pos_wts'])
            #print pos_wt
            #pos_wts.append(st.tag(separatewords(doc,0)))
            #print pos_wts
    return (sentenceLists, pos_wts)


def processLikelihoodRatios(articles, w1, sentenceLists=[], pos_wts=[]):
    #print w1
    w = []
    wt_corpus = []
    try:
        ngrm_len = len(w1.split("|"))
        if sentenceLists == [] or pos_wts == [] or len(sentenceLists) != len(pos_wts):
            sentenceLists, pos_wts = createSentencesAndPosTags(articles)
        for sentenceList, pos_wt_1 in zip(sentenceLists, pos_wts):
            index = 0
            for s in sentenceList:
                #Sentence loop is required as we don't want to pick up the keyphrases with words in different sentences
                #Otherwise there is NO NEED of sentences AT ALL
                #Added the POS tags to the phrases, will clear out on the basis of POS tags later
                #POS tags are added because trigrams will be using bigram information and hence we can not filter on the basis of pos tags.
                wt = separatewords(s, 0)
                pos_wt = pos_wt_1[index:index + len(wt)]
                index = index + len(pos_wt)
                for i in range(0, len(pos_wt) - ngrm_len + 1):
                    tmp = ""
                    for j in range(0, ngrm_len):
                        tmp = tmp + "|" + pos_wt[i + j][0].lower() + "(" + pos_wt[i + j][1] + ")"
                    if tmp[1:] == w1:
                        if i < len(pos_wt) - ngrm_len and pos_wt[i + ngrm_len] not in stplst and pos_wt[i + ngrm_len][0].lower().find("infoasmtestentity") < 0:
                            w.append(w1 + "|" + pos_wt[i + ngrm_len][0].lower() + "(" + pos_wt[i + ngrm_len][1] + ")")
                        if i != 0 and pos_wt[i - 1] not in stplst and pos_wt[i-1][0].lower().find("infoasmtestentity") < 0:
                            w.append(pos_wt[i - 1][0].lower() + "(" + pos_wt[i - 1][1] + ")" + "|" + w1)
                for p in pos_wt:
                    wt_corpus.append(p[0].lower() + "(" + p[1] + ")")
    except:
        print "Buffer Size Issue"
    w = list(set(w))
    return computeLikelihoodScores(w, wt_corpus)


def computeLikelihoodScores(w, wt_corpus):
    #Collocation shouldnt be disturbed with the inclusion of POS in the list of words. If "here" is treated as a verb somewhere
    #and as a noun somewhere becuase of bad POS tagging then it should not treat it differently in the collocation scores
    #The best would be to remove (*) and then compare the words to calculate if an n gram is a collocation or not
    phrases = []
    N = float(len(wt_corpus))
    for w12 in w:
        w1 = w12.split("|")[0]
        w1_l = len(w1.split())
        w2 = w12.split("|")[1]
        w2_l = len(w2.split())
        c1 = 0
        c2 = 0
        c12 = 0

        for i in range(0, len(wt_corpus)):
            tmp1 = ""
            for j in range(0, min(w1_l, len(wt_corpus) - i)):
                tmp1 = tmp1 + " " + wt_corpus[i + j]
            if tmp1.strip() == w1:
                c1 += 1
            tmp2 = ""
            for j in range(0, min(w2_l, len(wt_corpus) - i)):
                tmp2 = tmp2 + " " + wt_corpus[i + j]
            if tmp2.strip() == w2:
                c2 += 1
            tmp12 = ""
            for j in range(0, min(w1_l + w2_l, len(wt_corpus) - i)):
                tmp12 = tmp12 + " " + wt_corpus[i + j]
            if tmp12.strip() == w1 + " " + w2:
                c12 += 1
        try:
            p = c2 / N
            p1 = c12 / c1
            p2 = (c2 - c12) / (N - c1)
            #print w12 + " " + str(p1) + " " + str(p2) + " " + str(p2) + " " + str(N)
            score = -2 * math.log(
                bino(c12, c1, p) * bino(c2 - c12, N - c1, p) / bino(c12, c1, p1) / bino(c2 - c12, N - c1, p2))
            phrases.append([w12, score])
        except:
            if 1 == 0: print w12 + " computation error"
    return phrases


def bino(k, n, x):
    return math.pow(x, k) * math.pow(1.0 - x, n - k)


def insertKeyphrasesNgrams(article):
    phrases_to_keep = []
    article_id = article['_id']
    phrases = article['keyphrases_2grams']
    phrases.extend(article['keyphrases_3grams'])
    phrases.extend(article['keyphrases_4grams'])
    try:
        print article_id, "started!"
        #api_key = 'AIzaSyB4RxyL-cEewgxcTumD92BvHfgTkz4Z4FE'
        api_key = 'AIzaSyBpY1LHR9hQ-wS7O9gI7tcy-vov4w3Er6k'
        for (phrase, score) in phrases:
            if len(phrase.split("|")) == 2 and phrase[-4:] != "(RB)" and not any(
                            s in phrase for s in ['(VB', '(C', '(PR', '(TO', '(IN', '(FW', '(RBR', '(RBS', '(MD',
                                                  '(DT', '(WDT', '(:']):
                phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))

            if len(phrase.split("|")) == 3:
                if "(NN" in phrase.split("|")[0] and "(NN" in phrase.split("|")[1] and "(NN" in phrase.split("|")[2]:
                    phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))
                elif "(NN" in phrase.split("|")[0] and "(JJ" in phrase.split("|")[1] and "(NN" in phrase.split("|")[2]:
                    phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))
                elif "(JJ" in phrase.split("|")[0] and "(NN" in phrase.split("|")[1] and "(NN" in phrase.split("|")[2]:
                    phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))
                elif "(JJ" in phrase.split("|")[0] and "(JJ" in phrase.split("|")[1] and "(NN" in phrase.split("|")[2]:
                    phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))
                elif "(NN" in phrase.split("|")[0] and "(NN" in phrase.split("|")[2]:
                    query = phrase.replace("|", " ")
                    query = re.sub("\(.*?\)", "", query)
                    service_url = 'https://www.googleapis.com/freebase/v1/search'
                    params = {
                        'query': query,
                        'key': api_key
                    }
                    url = service_url + '?' + urllib.urlencode(params)
                    response = json.loads(urllib.urlopen(url).read())
                    #print response
                    for result in response['result']:
                        if query.strip().lower() == result['name'].strip().lower():
                            print query
                            #print result['name'] + ' (' + str(result['score']) + ')'
                            phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))
                            break

            if len(phrase.split("|")) > 3:
                query = phrase.replace("|", " ")
                query = re.sub("\(.*?\)", "", query)
                service_url = 'https://www.googleapis.com/freebase/v1/search'
                params = {
                    'query': query,
                    'key': api_key
                }
                url = service_url + '?' + urllib.urlencode(params)
                response = json.loads(urllib.urlopen(url).read())
                for result in response['result']:
                    if query.strip().lower() == result['name'].strip().lower():
                        #print query
                        #print result['name'] + ' (' + str(result['score']) + ')'
                        phrases_to_keep.append((" ".join(re.findall("(\w+)\(", phrase)), score))
                        break
                        #DBUtils.update_keyphrases(db, article_id, phrases_to_keep)
    except BaseException as e:
        print "Exception!!!", e
        print article_id, "Fucked!"
    else:
        print article_id, "Done!"
    finally:
        return phrases_to_keep


def get_sane_content(struct_data):
    text = ""
    for para in struct_data['paras']:
        if para['head'] != -1:
            text += struct_data['heads'][para['head']] + "\n"
        text += " ".join(para['cont']) + '\n'
    return text


def process_article_ngrms(article, ngrm):
    print "received", article["_id"]
    to_ret = []

    try:
        for n in range(2, int(ngrm) + 1):

            doc = get_sane_content(article['content'])

            if len(doc) > 0:
                print str(article['_id']) + "  " + str(n) + "grm"

                a = [article]
                sentenceLists, pos_wts = createSentencesAndPosTags(a)

                w = []
                if n == 2:
                    if len(pos_wts) > 0:
                        pos_wt = pos_wts[0]
                    else:
                        pos_wt = []
                    for i in range(0, len(pos_wt) - 1):
                        if pos_wt[i][0] != '' and pos_wt[i][0].lower() != "infoasmtestentity":#and pos_wt[i][1][:2] not in ('VB','IN','CC','TO','MD'):
                            w.append(pos_wt[i][0].lower() + "(" + pos_wt[i][1] + ")")#+" "+pos_wt[i+1][0])
                    w = list(set(w))
                else:
                    try:
                        Nminus1_grams = article['keyphrases_' + str(n - 1) + 'grams']
                        for b in Nminus1_grams:
                            w.append(b[0].replace("u'", "").replace("'", ""))
                    except:
                        w = []

                w = list(set(w))
                phrases = []
                for w1 in w:
                    phrases.extend(processLikelihoodRatios(a, w1, sentenceLists, pos_wts))
                p_set = set(tuple(x) for x in phrases)
                phrases = [list(x) for x in p_set]
                phrases.sort(key=lambda phrases: phrases[1], reverse=True)

                #for phrase in phrases:
                #    print phrase
                #    db.articles.update({'_id': article['_id']}, {"$push": {'keyphrases_' + str(n) + 'grams': phrase}}, upsert=True)

                article['keyphrases_' + str(n) + 'grams'] = phrases

        article['keyphrases'] = insertKeyphrasesNgrams(article)
        to_ret.append((article["_id"], article["keyphrases"]))

    except BaseException as e:
        print "Hola! Something is wrong", e, article["_id"]
        to_ret.append((article["_id"], []))
    else:
        print "Finally done", article["_id"]
    finally:
        return to_ret


final_results = []


def log_result(result):
    print result
    try:
        db = DBUtils.connect_db()
        DBUtils.update_keyphrases(db, result[0][0], result[0][1])
        print result[0]
    except BaseException as e:
        print "What to do: ", e

    return


#def update_to_db(db, final_results):
#    for res in final_results:
#        print "Updated", res[0]
#        DBUtils.update_keyphrases(db, res[0], res[1])
#    return


def main():
    import optparse

    parser = optparse.OptionParser()
    parser.add_option("-f", dest="filename", help="corpus filename")
    parser.add_option("-d", dest="coll", help="collection yes or no")
    parser.add_option("-s", dest="switch", help="Y if corpus wide phrases are to be selected")
    parser.add_option("-n", dest="ngrm", help="which n gram to select")
    (options, args) = parser.parse_args()

    if options.filename:
        f = open(options.filename, 'r')
        text = f.read()
        print text
        f.close()
        #processMeanVariance(articles) # What the hell is this?
    elif options.coll == 'Y':
        print "loading from collection"

        db = DBUtils.connect_db()
        articles = db.articles

        proc_Pool = Pool(processes=8)
        print articles

        #processMeanVariance(articles)
        if options.switch == 'Y':
            #f = open("lda_words_list","r")
            #line = f.readline()
            #f.close
            #w = line.split(",")
            w = []
            sentenceLists, pos_wts = createSentencesAndPosTags(articles.find())
            for pos_wt in pos_wts:
                for i in range(0, len(pos_wt) - 1):
                    if pos_wt[i][0] != '': #and pos_wt[i][1][:2] not in ('VB','IN','CC','TO','MD'):
                        w.append(pos_wt[i][0] + "(" + pos_wt[i][1] + ")")
            w = list(set(w))
            print len(w)
            for w1 in w:
                processLikelihoodRatios(articles.find(), w1, sentenceLists, pos_wts)
        else:
            for article in articles.find({"keyphrases": {"$exists": False}}).limit(1000):
                print article["_id"]
                proc_Pool.apply_async(process_article_ngrms, args=(article, options.ngrm, ), callback=log_result)

            proc_Pool.close()
            proc_Pool.join()

            #update_to_db(db, final_results)
    else:
        print "Please provide the file"
        return 0


if __name__ == "__main__":
    main()

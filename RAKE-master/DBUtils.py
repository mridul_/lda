__author__ = 'Dipankar Patro'

from pymongo import MongoClient
import re


def connect_db():
    client = MongoClient()
    return client['iadb']


#<editor-fold desc="_id fetchers">
def get_org_id(orgs_coll, org, article_ref):
    org = re.sub("[ *]+Inc.*", "", org)
    res = orgs_coll.find_one({"label": org}, {"_id": 1})
    if res is None:
        orgs_coll.insert({"label": org, "articles": [{'_id': article_ref, 'relevance': 1.0}]})
        _id = orgs_coll.find_one({"label": org})['_id']
    else:
        _id = res['_id']
        orgs_coll.update({'_id': _id}, {'$addToSet': {'articles': {'_id': article_ref, 'relevance': 1.0}}},
                         upsert=False, multi=True)

    return _id


def get_people_id(people_coll, person, article_ref):
    res = people_coll.find_one({"name": person}, {"_id": 1})
    if res is None:
        people_coll.insert({"name": person, "articles": [{'_id': article_ref, 'relevance': 1.0}]})
        _id = people_coll.find_one({"name": person})['_id']
    else:
        _id = res['_id']
        people_coll.update({'_id': _id}, {'$addToSet': {'articles': {'_id': article_ref, 'relevance': 1.0}}},
                           upsert=False, multi=True)

    return _id


def get_loc_id(locs_coll, loc, article_ref):
    res = locs_coll.find_one({"name": loc}, {"_id": 1})
    if res is None:
        locs_coll.insert({"name": loc, "articles": [{'_id': article_ref, 'relevance': 1.0}]})
        _id = locs_coll.find_one({"name": loc})['_id']
    else:
        _id = res['_id']
        locs_coll.update({'_id': _id}, {'$addToSet': {'articles': {'_id': article_ref, 'relevance': 1.0}}},
                         upsert=False, multi=True)

    return _id


def get_phrase_id(keyphrases_coll, phrase, article_ref, score=1.0, ocscr=0):
    res = keyphrases_coll.find_one({"name": phrase}, {"_id": 1})
    if res is None:
        keyphrases_coll.insert({"name": phrase, "articles": [{'_id': article_ref, 'relevance': score, 'ocscr': ocscr}]})
        _id = keyphrases_coll.find_one({"name": phrase})['_id']
    else:
        _id = res['_id']
        keyphrases_coll.update({'_id': _id}, {'$addToSet': {'articles': {'_id': article_ref, 'relevance': score, 'ocscr': ocscr}}},
                         upsert=False, multi=True)
    return _id
#</editor-fold>


def update_orgs(db, article_id, orgs):
    org_refs = [{"_id": get_org_id(db.orgs, org, article_id), "ocscr": orgs.count(org)} for org in orgs]
    db.articles.update({"_id": article_id}, {"$set": {"orgs": org_refs}}, upsert=True, multi=True)


def update_people(db, article_id, people):
    pers_ref = [{"_id": get_people_id(db.people, pers, article_id), "ocscr": people.count(pers)} for pers in set(people)]
    db.articles.update({"_id": article_id}, {"$set": {"people": pers_ref}}, upsert=True, multi=True)


def update_locs(db, article_id, locs):
    loc_refs = [{"_id": get_loc_id(db.locations, loc, article_id), "ocscr": locs.count(loc)} for loc in set(locs)]
    db.articles.update({"_id": article_id}, {"$set": {"locs": loc_refs}}, upsert=True, multi=True)


def update_keyphrases(db, article_id, phrases):
    phrase_refs = [{"_id": get_phrase_id(db.keyphrases, phrase[0], article_id, phrase[1], phrases.count(phrase)), "ocscr": phrases.count(phrase)}
                   for phrase in set(phrases)]
    db.articles.update({"_id": article_id}, {"$set": {"keyphrases": phrase_refs}}, upsert=True, multi=False)